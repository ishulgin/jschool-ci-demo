#!/bin/sh

set -e

host=ec2-18-159-248-4.eu-central-1.compute.amazonaws.com
connection_url=ec2-user@$host

if [ -n "$GITLAB_CI" ]; then
  mkdir -p ~/.ssh && touch ~/.ssh/known_hosts
  ssh-keyscan -t rsa $host >> ~/.ssh/known_hosts
  chmod 400 "$SSH_PRIVATE_KEY"
fi

scp -i "$SSH_PRIVATE_KEY" target/demo.jar $connection_url:/home/ec2-user/app/demo.jar
ssh -i "$SSH_PRIVATE_KEY" $connection_url 'killall java || true'
ssh -i "$SSH_PRIVATE_KEY" $connection_url 'nohup java -jar /home/ec2-user/app/demo.jar > /dev/null 2>&1 &'

echo "Deployed to http://$host:8080"