package com.example.demo;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class S3Service {
    private final S3Config s3Config;

    private final AmazonS3 s3Client;

    public S3Service(S3Config s3Config) {
        this.s3Config = s3Config;
        AmazonS3ClientBuilder s3ClientBuilder = AmazonS3ClientBuilder.standard();
        if (s3Config.getUrl() != null) {
            s3ClientBuilder.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(s3Config.getUrl(), s3Config.getRegion()));
        } else {
            s3ClientBuilder.withRegion(s3Config.getRegion());
        }

        s3Client = s3ClientBuilder
                .withPathStyleAccessEnabled(true)
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(s3Config.getClientId(), s3Config.getClientSecret())))
                .build();
    }

    @PostConstruct
    public void init() {
        if (!bucketExists(s3Config.getBucket())) {
            AccessControlList bucketAcl = new AccessControlList();
            Grant grantAllUsers = new Grant(GroupGrantee.AllUsers, Permission.Read);
            Grant grantOwner = new Grant(new CanonicalGrantee(s3Client.getS3AccountOwner().getId()), Permission.FullControl);
            bucketAcl.grantAllPermissions(grantAllUsers, grantOwner);

            CreateBucketRequest createBucketRequest = new CreateBucketRequest(s3Config.getBucket());
            s3Client.createBucket(createBucketRequest);
        }
    }

    public String generateFile() {
        String uuid = UUID.randomUUID().toString();

        byte[] message = ("Hello from " + uuid).getBytes();
        ByteArrayInputStream contentsAsStream = new ByteArrayInputStream(message);
        ObjectMetadata md = new ObjectMetadata();
        md.setContentLength(message.length);
        s3Client.putObject(s3Config.getBucket(), uuid, contentsAsStream, md);

        return uuid;
    }

    public List<String> list() {
        return s3Client.listObjects(s3Config.getBucket()).getObjectSummaries().stream().map(S3ObjectSummary::getKey).collect(Collectors.toList());
    }

    private boolean bucketExists(String name) {
        return s3Client.doesBucketExistV2(name);
    }
}
