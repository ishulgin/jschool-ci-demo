package com.example.demo;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Component
public class S3Config {
    @Value("${s3.url}")
    private String url;
    @Value("${s3.region}")
    private String region;

    @Value("${s3.bucket}")
    private String bucket;
    @Value("${s3.clientId}")
    private String clientId;
    @Value("${s3.clientSecret}")
    private String clientSecret;
}
