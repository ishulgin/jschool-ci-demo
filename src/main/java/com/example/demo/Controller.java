package com.example.demo;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class Controller {

    private final S3Service s3Service;

    @GetMapping ("/")
    public String hello() {
        return "hello";
    }

    @GetMapping ("/create")
    public String create() {
        return s3Service.generateFile();
    }

    @GetMapping ("/list")
    public List<String> list() {
        return s3Service.list();
    }
}
