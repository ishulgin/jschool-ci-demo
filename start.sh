#!/bin/sh

sh cleanup.sh

set -e

export $(cat .env | xargs)

docker network create demo-app

docker build -t demo-app .

docker run -d --network=demo-app\
 -e MINIO_REGION_NAME="eu-central1"\
 -e MINIO_ACCESS_KEY="$S3_CLIENT_ID"\
 -e MINIO_SECRET_KEY="$S3_CLIENT_SECRET"\
 -p 9000:9000\
 --net-alias minio\
 --name minio\
 bitnami/minio:2021.2.11

docker run -d --network=demo-app\
 -e S3_URL="http://minio:9000"\
 -e S3_CLIENT_ID="$S3_CLIENT_ID"\
 -e S3_CLIENT_SECRET="$S3_CLIENT_SECRET"\
 -p 8080:8080\
 --net-alias app\
 --name demo-app\
 demo-app