FROM openjdk:13-alpine

EXPOSE 8080

COPY target/demo.jar /app/
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app/demo.jar"]