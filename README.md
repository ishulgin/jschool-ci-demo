По вопросам писать в tg [@antmog](https://t.me/antmog) или [@ishulgin](https://t.me/ishulgin)

### Docker workflow
```shell
mvn clean package
./start.sh
```

### Docker compose workflow
```shell
mvn clean package
docker-compose up --build
```

qqq